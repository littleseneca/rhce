#!/opt/homebrew/bin/python3
import subprocess
build_list = [
    "doctl",
    "compute",
    "droplet",
    "list",
    "--format",
    "ID",
    "--tag-name",
    "rhce"
]
rhce_drops = subprocess.run(build_list, capture_output=True, text=True) 
rhce_convert = str(rhce_drops.stdout)
rhce_list = rhce_convert.splitlines()
del rhce_list[0]
for i in rhce_list:
    subprocess.run(["doctl","compute","droplet-action","power-on", i], capture_output=True, text=True) 