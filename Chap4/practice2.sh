#!/bin/bash
ansible all -m yum -a "name=httpd state=removed"
ansible all -m systemd -a "name=httpd state=stopped enabled=false"
