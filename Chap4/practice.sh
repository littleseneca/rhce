#!/bin/bash
ansible all -m yum -a "name=httpd state=latest"
ansible all -m command -a "systemctl daemon-reload"
ansible all -m systemd -a "name=httpd state=started enabled=true"
